(function() {
  'use strict';

  angular
    .module('tripPlanner', [
      'ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngMessages',
      'ngAria', 'ngResource', 'ui.router', 'ui.bootstrap',
      'tp.form', 'tp.service']);

})();
