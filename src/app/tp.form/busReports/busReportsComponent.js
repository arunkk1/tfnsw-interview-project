(function () {
    'use strict';
    angular.module('tp.form', ['ngSanitize']).component('busReportsForm', {
        templateUrl: 'app/tp.form/busReports/busReportsComponent.html',
        controller: BusReportsComponent,
        controllerAs: 'busReportsComponentVm'
    }).filter('routeFilter', [function(routeFilterFunction) {
        function routeFilterFunction(route) {
            if(undefined != route && route.contains(' '))
            {
                return '<b>' + route.substring(0 , 3) + '</b>' + route.substring(3) ;
            } else if (undefined != route && route.contains('UNKNOWN')) {
                return 'Not Available';
            } else {
                return route;
            }
        }
        routeFilterFunction.$stateful = true;
        return routeFilterFunction;
    }]);

    BusReportsComponent.$inject = ['$scope', 'busDataService'];


    function BusReportsComponent($scope, busDataService) {
        var busReportsComponentVm = this;
        busDataService.getBusData().then(function (response) {
            busReportsComponentVm.busReportInfo = response.data;

        });

        $scope.getColor= function(deviationFromTimetable){
            if(undefined != deviationFromTimetable && deviationFromTimetable < 0){
                return "blue";
            }else if (undefined != deviationFromTimetable && deviationFromTimetable > 300){
                return "red";
            } else if (undefined != deviationFromTimetable && deviationFromTimetable > -1 && deviationFromTimetable < 301){
                return "green";
            }
            return "gray";
        };

        $scope.getStatus= function(deviationFromTimetable){
            if(undefined != deviationFromTimetable && deviationFromTimetable < 0){
                return "Early";
            }else if (undefined != deviationFromTimetable && deviationFromTimetable > 300){
                return "Late";
            } else if (undefined != deviationFromTimetable && deviationFromTimetable > 0 && deviationFromTimetable < 301){
                return "On Time";
            }
            return "Not Available";
        };
    }

})();
