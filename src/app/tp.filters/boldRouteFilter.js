(function () {
    'use strict';

    angular.module('tp.filters')
        .filter('boldRoute', boldRoute);

    boldRoute.$inject = [];

    function boldRoute() {
        return {
            filter: filter
        };

        function filter(busRoute) {
            return '<b>' + busRoute + '</b>';
        }
    }


})();