describe('boldRouteFilter', function () {
    'use strict';

    var filter;

    beforeEach(module('tp.filters'));

    beforeEach(function() {

        inject(function($injector) {
            filter = $injector.get('boldRouteFilter');
        })
    });

    describe('filter', function() {

        var busRoute = '891 2 1';
        var boldedBusRoute = '<b>891</b> 2 1';

       it('should make the first route bold', function() {
           var result = filter.filter(busRoute);
           expect(result).toBe(boldedBusRoute);
       });
    });
})